import subprocess
from threading import Thread
import time
import feedparser
from urlparse import urlparse
import mysql.connector
import config
import sys
from random import random


num_threads = 10
if len(sys.argv) >= 2:
    num_threads = int(sys.argv[1])

program_random = random()


def connect():
    for server in config.servers:
        try:
            db = mysql.connector.connect(host=server,
                                         user=config.user,
                                         passwd=config.password,
                                         database=config.database)
        except mysql.connector.Error as err:
            print "Note: can not connect to", server
            pass
        else:
            return db
    print "Error: can not connect to any server."
    return False


sql = """
UPDATE `rss` as t1, (SELECT `id`
               FROM   `rss`
               WHERE  ( CURRENT_TIMESTAMP - `datetime` > 10 )
                      AND `domain` IN (SELECT `domain`
                                       FROM   `rss`
                                       WHERE  CURRENT_TIMESTAMP - `datetime` > 1
                                      )
               ORDER  BY `datetime` ASC
               LIMIT  0, 1) as t2
SET    datetime = CURRENT_TIMESTAMP,
       `last_edit_id` = %s
WHERE t1.`id` = t2.`id`
"""


class Worker(Thread):
    def run(self):
        thread_id = str(program_random) + " " + str(random())
        # TODO: open connection to database and cursor
        db = connect()
        while not db:
            time.sleep(5)
            db = connect()

        cursor = db.cursor()

        while True:
            try:
                cursor.execute(sql, (thread_id,))
                cursor.execute('SELECT * FROM `rss` WHERE `last_edit_id` = %s  ORDER BY `datetime` DESC LIMIT 0, 1',
                               (thread_id,))
                row = cursor.fetchone()
                db.commit()
                if row != None:
                    url = row[2]

                    feed = feedparser.parse(url)
                    for entry in feed.entries:
                        parsed_uri = urlparse(entry.link)
                        domain = parsed_uri.netloc
                        try:
                            cursor.execute(
                                "INSERT INTO `rss`.`rss` (`id`, `type`, `url`, `domain`, `datetime`, `content`, `last_edit_id`) VALUES (NULL, 'webpage', %s, %s, CURRENT_TIMESTAMP, NULL, '')",
                                (entry.link, domain,)
                            )
                        except mysql.connector.Error as err:
                            pass
                        db.commit()
                        # # TODO: select by link
                        # db_entry = entry.link
                        # not_in_db = True

                        # if not_in_db or entry.published_parsed != db_entry.published:
                        #     # wget...
                        #     proc = subprocess.Popen(["wget --wait=1 --quiet --output-document=- " + entry.link],
                        #                             stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                        #                             stderr=subprocess.PIPE, shell=True)
                        #     (out, err) = proc.communicate()
                        #     print(out)
                        #     if not_in_db:
                        #         parsed_uri = urlparse(entry.link)
                        #         domain = parsed_uri.netloc
                        #         # insert
                        #         pass
                        #     else:
                        #         # update
                        #         pass
                        #     # commit
                else:
                    time.sleep(1)
            except mysql.connector.Error as err:
                pass

min_threads = 1
max_threads = 100

if num_threads < min_threads:
    num_threads = min_threads
elif num_threads > max_threads:
    num_threads = max_threads

threads = []
for i in range(num_threads - 1):
    threads.append(Worker())
    threads[i].start()
    if i % 20 == 0:
        time.sleep(1)

Worker().run()
