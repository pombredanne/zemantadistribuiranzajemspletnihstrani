import sys
from urlparse import urlparse
import mysql.connector
import config


def connect():
    for server in config.servers:
        try:
            db = mysql.connector.connect(host=server,
                                         user=config.user,
                                         passwd=config.password,
                                         database=config.database)
        except mysql.connector.Error as err:
            print "Note: can not connect to", server
            pass
        else:
            return db
    print "Error: can not connect to any server."
    return False


if len(sys.argv) >= 2:
    db = connect()
    if db:
        cursor = db.cursor()
        for line in open(sys.argv[1]):
            line = line.strip()
            parsed_uri = urlparse(line)
            domain = parsed_uri.netloc
            url = parsed_uri.geturl()
            try:
                cursor.execute(
                    "INSERT INTO `rss`.`rss` (`id`, `type`, `url`, `domain`, `datetime`, `content`, `last_edit_id`) VALUES (NULL, 'rss', %s, %s, CURRENT_TIMESTAMP, NULL, '')",
                    (url, domain,)
                )
            except mysql.connector.Error as err:
                pass
        db.commit()
else:
    print("Usage: python add_rss_feeds.py rss_urls.txt")
