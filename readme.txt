tested on Ubuntu 12.04

=== MariaDB with galera ===

Add repository:
https://downloads.mariadb.org/mariadb/repositories/#mirror=netcologne&distro=Ubuntu&distro_release=precise&version=5.5

sudo apt-get update
sudo apt-get install mariadb-galera-server

/etc/mysql/my.ini:
================================================================
[mysqld]
bind-address		= 0.0.0.0

binlog_format=ROW
innodb_autoinc_lock_mode=2
innodb_doublewrite=1
query_cache_size=0

wsrep_provider=/usr/lib/galera/libgalera_smm.so
wsrep_provider_options="gcache.size=256M; gcache.page_size=128M"
wsrep_cluster_address=gcomm://10.0.2.4,10.0.2.7,10.0.2.6
wsrep_cluster_name="my_little_cluster"
wsrep_node_address="10.0.2.4"
wsrep_node_name="node1"
wsrep_sst_method=rsync
wsrep_sst_auth="root:root"
wsrep_node_incoming_address=10.0.2.4
wsrep_sst_receive_address=10.0.2.4
wsrep_slave_threads=16
================================================================


sudo service mysql stop

First node:
sudo mysqld --wsrep-new-cluster --wsrep_cluster_address=gcomm://

Other nodes:
sudo mysqld



===



CREATE TABLE IF NOT EXISTS `rss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `url` varchar(500) NOT NULL,
  `domain` varchar(200) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` blob,
  `last_edit_id` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `domain` (`domain`,`datetime`),
  KEY `last_edit_id` (`last_edit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;




pip install -r requirements.txt


Add rss feeds into database (one url per line):
python add_rss_feeds.py rss_urls.txt

Start fetching feeds (with optional number of threads):
python rss_parser.py
python rss_parser.py 20
